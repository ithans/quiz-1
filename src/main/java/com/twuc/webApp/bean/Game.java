package com.twuc.webApp.bean;

public class Game {
    private Integer id;
    private String answer;

    public Game(Integer id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public Game() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    //{ "id": {gameId}, "answer": "{4 digits}"}
    @Override
    public String toString() {
        return "{" +
                "\"id\":" + id +
                ", \"answer\":" + "\"" + answer + "\"" +
                '}';
    }
}
