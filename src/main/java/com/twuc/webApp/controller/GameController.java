package com.twuc.webApp.controller;

import com.twuc.webApp.bean.Game;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Max;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
public class GameController {
    @PostMapping("/api/games")
    public ResponseEntity<String> startGame(HttpSession session) {
        ServletContext servletContext = session.getServletContext();
        Integer gameId = (Integer) servletContext.getAttribute("gameId");
        Map<Integer, String> map;
        if (gameId == null) {
            gameId = 1;
            map = new HashMap<Integer, String>();
            servletContext.setAttribute("gameId", gameId);
        } else {
            map = (Map<Integer, String>) servletContext.getAttribute("games");
            gameId++;
            servletContext.setAttribute("gameId", gameId);
        }
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() < 4) {
            int num = random.nextInt(10);
            if (sb.indexOf(num + "") == -1) {
                sb.append(num);
            }
        }
        String result = sb.toString();
        map.put(gameId, result);
        servletContext.setAttribute("games", map);
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", "/api/games/" + gameId)
                .build();

    }


    @GetMapping("/api/games/{gameId}")
    public ResponseEntity getGameStatus(@PathVariable Integer gameId, HttpSession session) {
        ServletContext servletContext = session.getServletContext();
        Map<Integer, String> map = (Map<Integer, String>) servletContext.getAttribute("games");
        if (gameId > (Integer) servletContext.getAttribute("gameId")) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        String result = map.get(gameId);
        Game game = new Game(gameId, result);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(game.toString());

    }

    @PatchMapping("/api/games/{gameId}")
    public ResponseEntity guestNum(@PathVariable Integer gameId, HttpSession session, @RequestBody Game game) {
        ServletContext servletContext = session.getServletContext();
        if (gameId < 0 || gameId > (Integer) servletContext.getAttribute("gameId")) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            Map<Integer, String> map = (Map<Integer, String>) servletContext.getAttribute("games");
            String answer = map.get(gameId);
            int A = 0;
            int B = 0;
            for (int i = 0; i < 4; i++) {
                if (game.getAnswer().charAt(i) == answer.charAt(i)) {
                    A++;
                }
                for (int j = 0; j < 4; j++) {
                    if (game.getAnswer().charAt(i) == answer.charAt(j)) {
                        B++;
                    }
                }
            }
            B = B - A;
            String hint = A + "A" + B + "B";
            boolean correct = A == 4 ? true : false;
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{\"hint\":" + hint + ",\"correct\":" + correct + "}");
        }
    }

}
