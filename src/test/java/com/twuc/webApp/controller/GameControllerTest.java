package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.awt.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void return_true_status_code() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/1"));
    }

    @Test
    void return_true_status_code_new() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "/api/games/2"));
    }

    @Test
    void get_game_status() throws Exception {
        mockMvc.perform(get("/api/games/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void get_game_status_notfound() throws Exception {
        mockMvc.perform(get("/api/games/3"))
                .andExpect(status().isNotFound());
    }

    @Test
    void text_process_guest_name() throws Exception {
        mockMvc.perform(patch("/api/games/1")
                .content("{\"answer\":\"1234\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
    @Test
    void text_process_guest_name_status_404() throws Exception {
        mockMvc.perform(patch("/api/games/3")
                .content("{\"answer\":\"1234\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
